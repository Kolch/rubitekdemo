//
//  OpenDoorButtonView.swift
//  RubitekDemo
//
//  Created by Alex Kolchedantsev on 11.02.2021.
//

import Foundation
import UIKit

class OpenDoorButtonView: UIView {
    
    @IBOutlet weak var contentView: UIView!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("OpenDoorButtonView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        contentView.backgroundColor = .white
        contentView.layer.cornerRadius = 15
    }
}

