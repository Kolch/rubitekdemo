//
//  SegmentedControl.swift
//  RubitekDemo
//
//  Created by Alex Kolchedantsev on 09.02.2021.
//

import Foundation
import UIKit
import RxSwift

enum SegmentedControlState: Int {
    case cameras = 0, doors = 1
}

class SegmentedControl: UIView {
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var selectionView: UIView!
    @IBOutlet weak var constraintOne: NSLayoutConstraint!
    @IBOutlet weak var constraintTwo: NSLayoutConstraint!
    let bag = DisposeBag()
    
    var state = PublishSubject<SegmentedControlState>()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("SegmentedControl", owner: self, options: nil)
        addSubview(contentView)
        subscribeToState()
        
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        selectionView.layer.cornerRadius = 2
        selectionView.backgroundColor = Colors.mainBlue
        self.addShadow()
        
        constraintOne.priority = UILayoutPriority(rawValue: 950)
    }
    
    @IBAction func buttonOne(_ sender: UIButton) {
        state.onNext(.cameras)
    }
    
    @IBAction func buttonTwo(_ sender: UIButton) {
        state.onNext(.doors)
    }
    
    private func subscribeToState() {
        state.subscribe { [self] in
            guard let state = $0.element else { return }
            
            constraintOne.priority = UILayoutPriority(rawValue: state == .cameras ? 950 : 750)
            constraintTwo.priority = UILayoutPriority(rawValue: state == .cameras ? 750 : 950)
            UIView.animate(withDuration: 0.3) {
                self.contentView.layoutIfNeeded()
            }
        }.disposed(by: bag)
    }
}
