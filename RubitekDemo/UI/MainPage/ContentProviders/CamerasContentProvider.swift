//
//  CamerasTableDelegate.swift
//  RubitekDemo
//
//  Created by Alex Kolchedantsev on 10.02.2021.
//

import Foundation
import UIKit
import RxSwift

final class CamerasContentProvider: TableViewDelegate, ContentProviderProtocol {
    
    let cellNibName: String = "CamerasTableViewCell"
    var bag: DisposeBag = DisposeBag()
    var networkService = NetworkService()
    var storageService = StorageService()
    var state = PublishSubject<ContentProviderState>()
    var didSelectItem = PublishSubject<TableViewContent>()
    
    override var dataSource: [[TableViewContent]] {
        didSet {
            sendReloadNotification()
        }
    }
    
    override init() {
        super.init()
        retrieveDataFromStorage()
    }
    
    private func retrieveDataFromStorage() {
        state.onNext(.loading)
        
        storageService.retrieveCameras()
            .subscribe {
                self.storageResponceHandler($0)
            }
            .disposed(by: bag)
            
    }
    
    func fetchDataFromApi() {
        state.onNext(.loading)
        
        networkService.fetchCameras()
            .subscribe {
                self.networkResponceHandler($0)
            }
            .disposed(by: bag)
    }
    
    @objc func refreshData() {
        fetchDataFromApi()
    }
    
    private func favoriteButtonAction(at indexPath: IndexPath) {
        let object = dataSource[indexPath.section][indexPath.row]
        storageService.toggleFavorite(object)
            .subscribe()
            .disposed(by: bag)
    }
}

extension CamerasContentProvider {
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {

        let actionhandler: UIContextualAction.Handler = { [weak self] (action, button, completion) in
            self?.favoriteButtonAction(at: indexPath)
            completion(true)
        }

        let action = UIContextualAction(style: .normal, title: "", handler: actionhandler)

        action.backgroundColor = Colors.mainBackground
        action.image = UIImage(named: "star")

        return UISwipeActionsConfiguration(actions: [action])
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let object = dataSource[indexPath.section][indexPath.row]
        didSelectItem.onNext(object)
    }
}
