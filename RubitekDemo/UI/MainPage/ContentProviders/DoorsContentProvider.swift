//
//  DoorsTableViewDelegate.swift
//  RubitekDemo
//
//  Created by Alex Kolchedantsev on 10.02.2021.
//

import Foundation
import UIKit
import RxSwift

final class DoorsContentProvider: TableViewDelegate, ContentProviderProtocol {

    let cellNibName: String = "DoorsTableViewCell"
    var bag: DisposeBag = DisposeBag()
    var networkService = NetworkService()
    var storageService = StorageService()
    var state = PublishSubject<ContentProviderState>()
    var didSelectItem = PublishSubject<TableViewContent>()
    
    override var dataSource: [[TableViewContent]] {
        didSet {
            sendReloadNotification()
        }
    }
    
    override init() {
        super.init()
        retrieveDataFromStorage()
    }
    
    private func retrieveDataFromStorage() {
        state.onNext(.loading)
        
        storageService.retrieveDoors()
            .subscribe {
                self.storageResponceHandler($0)
            }
            .disposed(by: bag)
            
    }
    
    func fetchDataFromApi() {
        state.onNext(.loading)
        
        networkService.fetchDoors()
            .subscribe {
                self.networkResponceHandler($0)
            }
            .disposed(by: bag)
    }
    
    @objc func refreshData() {
        fetchDataFromApi()
    }
    
    private func editAction(at indexPath: IndexPath) {
        let object = dataSource[indexPath.section][indexPath.row]
        askForRename(object: object)
    }
    
    private func changeObjectName(_ object: TableViewContent, newName: String) {
        storageService.changeName(object, newName: newName)
            .subscribe()
            .disposed(by: bag)
    }
    
    private func favoriteButtonAction(at indexPath: IndexPath) {
        let object = dataSource[indexPath.section][indexPath.row]
        storageService.toggleFavorite(object)
            .subscribe()
            .disposed(by: bag)
    }
    
    private func askForRename(object: TableViewContent) {
        let alert = UIAlertController(title: "Изменить название", message: nil, preferredStyle: .alert)
        alert.addTextField {
            $0.text = object.objectName
        }
        
        alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Изменть", style: .default, handler: { [self] _ in
            let textField = alert.textFields![0]
            guard let text = textField.text else { return } // No text somehow
            changeObjectName(object, newName: text)
            state.onNext(.normal)
        }))
        
        state.onNext(.alert(alert))
    }
}

extension DoorsContentProvider {
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {

        let favoriteActionHandler: UIContextualAction.Handler = { [weak self] action, button, completion in
            self?.favoriteButtonAction(at: indexPath)
            completion(true)
        }

        let editActionHandler: UIContextualAction.Handler = { [weak self] action, button, completion in
            self?.editAction(at: indexPath)
            completion(true)
        }

        let favoriteAction = UIContextualAction(style: .normal, title: "", handler: favoriteActionHandler)

        favoriteAction.backgroundColor = Colors.mainBackground
        favoriteAction.image = UIImage(named: "star")

        let editAction = UIContextualAction(style: .normal, title: "", handler: editActionHandler)

        editAction.backgroundColor = Colors.mainBackground
        editAction.image = UIImage(named: "edit")

        return UISwipeActionsConfiguration(actions: [favoriteAction, editAction])
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let object = dataSource[indexPath.section][indexPath.row]
        didSelectItem.onNext(object)
    }
}
