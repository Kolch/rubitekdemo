//
//  TableViewContentDelegateProtocol.swift
//  RubitekDemo
//
//  Created by Alex Kolchedantsev on 10.02.2021.
//

import Foundation

import UIKit
import RxSwift

enum ContentProviderState {
    case loading
    case normal
    case error(Error)
    case alert(UIAlertController)
}

protocol ContentProviderProtocol: TableViewDelegate {
    var cellNibName: String { get }
    var state: PublishSubject<ContentProviderState> { get set }
    var didSelectItem: PublishSubject<TableViewContent> { get set }
    var storageService: StorageService { get set }
    var networkService: NetworkService { get set }
    var bag: DisposeBag { get set }
    func fetchDataFromApi()
    func refreshData()
}

extension ContentProviderProtocol {
    
    func networkResponceHandler<T: NetworkResponce> (_ event: Event<T>) {
        switch event {
        case .completed:
            print("COMPLETED")
            state.onNext(.normal)
        case .error(let error):
            state.onNext(.error(error))
        case .next(let responce):
            networkValueHandler(responce: responce)
        }
    }
    
    func networkValueHandler(responce: NetworkResponce) -> Void  {
        dataSource = responce.data.chunckByRoomName()
        saveData(responce.data)
    }
    
    func storageResponceHandler<T: TableViewContent> (_ event: Event<[T]>) {
        switch event {
        case .completed:
            print("COMPLETED")
            state.onNext(.normal)
        case .error(_):
            fetchDataFromApi()
        case .next(let value):
            state.onNext(.normal)
            storageValueHandler(value: value)
        }
    }
    
    func storageValueHandler(value: [TableViewContent]) -> Void {
        dataSource = value.chunckByRoomName()
    }
    
    func saveData(_ data: [TableViewContent]) {
        data.forEach {
            storageService.saveObject($0)
                .subscribe {
                    switch $0 {
                    case .success(): print("Success saving the object")
                    case .failure(let error): print("Error saving the object: \(error)")
                    }
                }
                .disposed(by: bag)
        }
    }
    
    func sendReloadNotification() {
        NotificationCenter.default.post(Notifications.reloadTableView)
    }
}

struct Notifications {
    static var reloadTableView = Notification(name: .init("reloadTableView"))
}
