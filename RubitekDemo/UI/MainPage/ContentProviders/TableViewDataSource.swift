//
//  TableViewDataSource.swift
//  RubitekDemo
//
//  Created by Alex Kolchedantsev on 11.02.2021.
//

import Foundation
import UIKit

class DataSourceOwner: NSObject {
    var dataSource: [[TableViewContent]] = []
}

class TableViewDelegate: DataSourceOwner, UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource[section].count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = TableHeaderView()
        headerView.titleLabel.text = dataSource[section].first?.getHeaderString()
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CellsIdentifiers.tableViewContentCell) as! TableViewContentCell
        cell.configure(with: dataSource[indexPath.section][indexPath.row])
        return cell
    }
}
