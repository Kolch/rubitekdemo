//
//  CamerasTableViewCell.swift
//  RubitekDemo
//
//  Created by Alex Kolchedantsev on 10.02.2021.
//

import UIKit
import RxSwift

class CamerasTableViewCell: UITableViewCell, TableViewContentCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var starImageView: UIImageView!
    var bag = DisposeBag()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        containerView.addShadow()
        containerView.layer.cornerRadius = 10

        DispatchQueue.main.async {
            self.imgView.roundCorners(corners: [.topLeft, .topRight], radius: 10)
        }
    }
    
    func configure(with content: TableViewContent) {
        guard let camera = content as? CameraModel else { return }
        
        starImageView.isHidden = !camera.isFavorite
        titleLabel.text = camera.name
        imgView.fromURL(camera.snapshot ?? "", bag: bag)
        
        camera.isFavoritePublisher
            .observe(on: MainScheduler.instance)
            .subscribe { self.starImageView.isHidden = !$0 }
            .disposed(by: bag)
    }
    
    override func prepareForReuse() {
        bag = DisposeBag()
    }
}

