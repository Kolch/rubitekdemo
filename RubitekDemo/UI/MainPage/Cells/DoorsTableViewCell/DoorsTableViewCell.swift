//
//  DoorsTableViewCell.swift
//  RubitekDemo
//
//  Created by Alex Kolchedantsev on 10.02.2021.
//

import UIKit
import RxSwift

class DoorsTableViewCell: UITableViewCell, TableViewContentCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var imageHightConstaint: NSLayoutConstraint!
    @IBOutlet weak var starImageView: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    var bag = DisposeBag()
    
    var hasMediaContent: Bool = false {
        didSet {
            imageHightConstaint.constant = hasMediaContent ? 200 : 0
            layoutIfNeeded()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        containerView.addShadow()
        containerView.layer.cornerRadius = 10
        
        DispatchQueue.main.async {
            self.imgView.roundCorners(corners: [.topLeft, .topRight], radius: 10)
        }
    }
    
    
    func configure(with content: TableViewContent) {
        guard let door = content as? DoorModel else { return }
        
        starImageView.isHidden = !door.isFavorite
        titleLabel.text = door.name
        hasMediaContent = door.snapshot != nil
        imgView.fromURL(door.snapshot ?? "", bag: bag)
        
        door.isFavoritePublisher
            .observe(on: MainScheduler.instance)
            .subscribe { self.starImageView.isHidden = !$0 }
            .disposed(by: bag)
        
        door.namePublisher
            .observe(on: MainScheduler.instance)
            .subscribe { self.titleLabel.text = $0 }
            .disposed(by: bag)
    }
    
    override func prepareForReuse() {
        bag = DisposeBag()
    }
}
