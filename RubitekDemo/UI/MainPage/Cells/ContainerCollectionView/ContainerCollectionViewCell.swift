//
//  ContainerCollectionViewCell.swift
//  RubitekDemo
//
//  Created by Alex Kolchedantsev on 10.02.2021.
//

import UIKit
import RxSwift
import RxCocoa

enum ContainerCollectionViewCellState {
    case cameras, doors
}

class ContainerCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var tableView: UITableView!
    
    private let bag = DisposeBag()
    private var refreshControl = UIRefreshControl()
    
    var contentProvider: ContentProviderProtocol! {
        didSet {
            setUpTableView()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        NotificationCenter.default.addObserver(self, selector: #selector(reloadTableView),
                                               name: Notifications.reloadTableView.name,
                                               object: nil)
    }
    
    @objc func reloadTableView() {
        tableView.reloadData()
    }
    
    private func setUpTableView() {
        tableView.register(UINib(nibName: contentProvider.cellNibName, bundle: nil), forCellReuseIdentifier: CellsIdentifiers.tableViewContentCell)
        
        tableView.tableFooterView = UIView()
        tableView.separatorStyle  = .none
        tableView.backgroundColor = Colors.mainBackground
        tableView.rowHeight       = UITableView.automaticDimension
        tableView.delegate        = contentProvider
        tableView.dataSource      = contentProvider
        tableView.addSubview(refreshControl)
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
    }
    
    @objc func refresh(_ sender: AnyObject) {
        self.contentProvider.refreshData()
        self.refreshControl.endRefreshing()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: Notifications.reloadTableView.name, object: nil)
    }
}
