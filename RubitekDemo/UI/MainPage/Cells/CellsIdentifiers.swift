//
//  CellsIdentifiers.swift
//  RubitekDemo
//
//  Created by Alex Kolchedantsev on 10.02.2021.
//

import Foundation

struct CellsIdentifiers {
    static var containerCollectionViewCell = "containerCell"
    static var tableViewContentCell        = "tableViewContent"
}
