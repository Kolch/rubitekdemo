//
//  TableViewContentCellProtocol.swift
//  RubitekDemo
//
//  Created by Alex Kolchedantsev on 10.02.2021.
//

import Foundation
import UIKit

protocol TableViewContentCell: UITableViewCell {
    func configure(with content: TableViewContent)
}
