//
//  ViewController.swift
//  RubitekDemo
//
//  Created by Alex Kolchedantsev on 09.02.2021.
//

import UIKit
import RxSwift

class MainPageViewController: ViewController {
    
    @IBOutlet weak var segmentedControl: SegmentedControl!
    @IBOutlet weak var collectionView: UICollectionView!
    
    let contentProviders: [ContentProviderProtocol] = [CamerasContentProvider(), DoorsContentProvider()]
    let bag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.isHidden = true
        setUp()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        bindSegmentCtrlToCollectionView()
    }
    
    private func setUp() {
        setUpActivityIndicator()
        setUpCollectionView()
        subscribeOnContentProviders()
    }
    
    private func setUpCollectionView() {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: "ContainerCollectionViewCell", bundle: nil),
                                forCellWithReuseIdentifier: CellsIdentifiers.containerCollectionViewCell)
    }
    
    private func bindSegmentCtrlToCollectionView() {
        segmentedControl.state.subscribe(onNext:  {
            self.collectionView.scrollToItem(at: IndexPath(row: $0.rawValue, section: 0),
                                             at: .centeredHorizontally,
                                             animated: true)
        }).disposed(by: bag)
    }
    
    private func subscribeOnContentProviders() {
        contentProviders.forEach {
            observeContentProviderState(provider: $0)
            observeSelections(provider: $0)
        }
    }
    
    private func observeContentProviderState(provider: ContentProviderProtocol) {
        provider.state
            .observe(on: MainScheduler.instance)
            .subscribe(onNext:  { [weak self] in
                switch $0 {
                case .loading:
                    self?.loadingIndicator.startAnimating()
                case .normal:
                    self?.loadingIndicator.stopAnimating()
                case .error(let error):
                    self?.showError(error)
                case .alert(let alert):
                    self?.present(alert, animated: true, completion: nil)
                }
            })
            .disposed(by: bag)
    }
    
    private func observeSelections(provider: ContentProviderProtocol) {
        provider.didSelectItem
            .subscribe {
                self.navigateToVideoDetailPage(object: $0.element!)
            }
            .disposed(by: bag)
    }
    
    // TODO: move it to router or something
    
    private func navigateToVideoDetailPage(object: TableViewContent) {
        let storyboard = UIStoryboard(name: "VideoDetailPage", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "VideoDetailPage") as! VideoDetailPageViewController
       
        guard let door = object as? DoorModel, door.snapshot != nil else { return }
        vc.door = door
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension MainPageViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return contentProviders.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellsIdentifiers.containerCollectionViewCell,
                                                      for: indexPath) as! ContainerCollectionViewCell
        
        cell.contentProvider = contentProviders[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: view.frame.width, height: collectionView.frame.height)
    }
}
