//
//  VideoDetailPageViewController.swift
//  RubitekDemo
//
//  Created by Alex Kolchedantsev on 11.02.2021.
//

import UIKit
import RxSwift

class VideoDetailPageViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var bottomBarView: BottomBarView!
    @IBOutlet weak var buttonContainer: UIButton!
    var openDoorButtonView: OpenDoorButtonView!
    var door: DoorModel!
    let bag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Colors.mainBackground
        setUpOpenDoorButton()
        setUpContent()
        bottomBarView.backButton.addTarget(self, action: #selector(backButtonAction), for: .touchUpInside)
    }
    
    private func setUpOpenDoorButton() {
        openDoorButtonView = OpenDoorButtonView(frame: buttonContainer.bounds)
        openDoorButtonView.isUserInteractionEnabled = false
        buttonContainer.backgroundColor = .clear
        buttonContainer.addSubview(openDoorButtonView)
    }
    
    private func setUpContent() {
        titleLabel.text = door.name
        imgView.fromURL(door.snapshot ?? "", bag: bag)
    }
    
    @objc func backButtonAction() {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func openDoorButtonAction(_ sender: UIButton) {
        print("OPEN DOOR ACTION")
    }
}
