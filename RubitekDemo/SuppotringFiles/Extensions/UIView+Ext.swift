//
//  UIView+Ext.swift
//  RubitekDemo
//
//  Created by Alex Kolchedantsev on 09.02.2021.
//

import Foundation
import UIKit

extension UIView {
    func addShadow(opacity: Float = 0.2, radius: CGFloat = 1.0) {
        layer.shadowColor   = UIColor.black.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset  = .init(width: 0, height: 2)
        layer.shadowRadius  = radius
    }
    
    
    func roundCorners(corners:UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
}
