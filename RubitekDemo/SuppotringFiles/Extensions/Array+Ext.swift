//
//  Array+Ext.swift
//  RubitekDemo
//
//  Created by Alex Kolchedantsev on 11.02.2021.
//

import Foundation
import UIKit

extension Array where Element == TableViewContent {
    func chunckByRoomName() -> [[Element]] {
        Dictionary(grouping: self) { $0.roomName }.values
            .sorted(by: { $0.count < $1.count })
    }
}
