//
//  UIImageView+Ext.swift
//  RubitekDemo
//
//  Created by Alex Kolchedantsev on 10.02.2021.
//

import Foundation
import UIKit
import RxSwift


extension UIImageView {
    func fromURL(_ urlString: String, bag: DisposeBag) {
        guard let url = URL(string: urlString) else { return }
        loadImage(for: url)
            .subscribe {
                if let wrappedImage = $0.element, let image = wrappedImage {
                    self.image = image
                }
            }
            .disposed(by: bag)
    }
    
    func loadImage(for url: URL) -> Observable<UIImage?> {
        return ImageLoader.shared.loadImage(from: url)
    }
}
