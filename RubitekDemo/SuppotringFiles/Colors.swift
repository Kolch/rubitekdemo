//
//  Colors.swift
//  RubitekDemo
//
//  Created by Alex Kolchedantsev on 09.02.2021.
//

import Foundation
import UIKit

extension UIColor {
    convenience init(_ r: CGFloat, _ g: CGFloat, _ b: CGFloat, _ a: CGFloat = 1) {
        self.init(red: r/255, green: g/255, blue: b/255, alpha: a)
    }
}

struct Colors {
    static var mainBlue       = UIColor(3,169,244)
    static var mainBackground = UIColor(246,246,246)
}
