//
//  TableViewContentProtocol.swift
//  RubitekDemo
//
//  Created by Alex Kolchedantsev on 10.02.2021.
//

import Foundation
import RealmSwift

protocol TableViewContent: Object {
    var roomName: String? { get }
    var objectName: String? { get }
    
    func getHeaderString() -> String
    func toggleFavorite()
    func changeName(to newName: String)
}
