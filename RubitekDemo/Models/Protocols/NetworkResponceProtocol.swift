//
//  NetworkResponceProtocol.swift
//  RubitekDemo
//
//  Created by Alex Kolchedantsev on 10.02.2021.
//

import Foundation

protocol NetworkResponce: Codable {
    var data: [TableViewContent] { get }
}
