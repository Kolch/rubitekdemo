//
//  DoorResponce.swift
//  RubitekDemo
//
//  Created by Alex Kolchedantsev on 10.02.2021.
//

import Foundation

struct DoorsResponce: Codable, NetworkResponce {
    var data: [TableViewContent] { doors }
    
    var success: Bool
    var doors: [DoorModel]
    
    enum CodingKeys: String, CodingKey {
        case success
        case doors = "data"
    }
}
