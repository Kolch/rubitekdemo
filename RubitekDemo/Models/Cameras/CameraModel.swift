//
//  CameraModel.swift
//  RubitekDemo
//
//  Created by Alex Kolchedantsev on 10.02.2021.
//

import Foundation
import RealmSwift
import RxSwift

class CameraModel: Object, TableViewContent, Codable {
    
    var roomName: String? { room }
    var objectName: String? { name }
    var isFavoritePublisher = PublishSubject<Bool>()
    
    @objc dynamic var id: Int = 0
    @objc dynamic var name: String?
    @objc dynamic var snapshot: String?
    @objc dynamic var room: String?
    @objc dynamic var rec: Bool = false
    @objc dynamic var isFavorite: Bool = false
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case snapshot
        case room
        case isFavorite = "favorites"
        case rec
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func getHeaderString() -> String {
        return room ?? "Default room"
    }
    
    func toggleFavorite() {
        isFavorite.toggle()
        isFavoritePublisher.onNext(isFavorite)
    }
    
    func changeName(to newName: String) {}
}
