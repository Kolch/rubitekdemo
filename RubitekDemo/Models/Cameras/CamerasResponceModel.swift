//
//  CamerasResponceModel.swift
//  RubitekDemo
//
//  Created by Alex Kolchedantsev on 10.02.2021.
//

import Foundation

struct CamerasResponce: Codable, NetworkResponce {
    var data: [TableViewContent] { cameras }
    
    var success: Bool
    var room: [String]
    var cameras: [CameraModel]
    
    private enum RootCodingKeys: String, CodingKey {
        
        case data = "data"
        case success
        
        enum NestedCodingKeys: String, CodingKey {
            case cameras
            case room
        }
    }
    
    public init(from decoder: Decoder) throws {
        
        let rootContainer = try decoder.container(keyedBy: RootCodingKeys.self)
        self.success      = try rootContainer.decode(Bool.self, forKey: .success)
        
        let dataContainer = try rootContainer.nestedContainer(keyedBy: RootCodingKeys.NestedCodingKeys.self, forKey: .data)
        self.room         = try dataContainer.decode([String].self, forKey: .room)
        self.cameras      = try dataContainer.decode([CameraModel].self, forKey: .cameras)
    }
}
