//
//  StorageService.swift
//  RubitekDemo
//
//  Created by Alex Kolchedantsev on 10.02.2021.
//

import Foundation
import RealmSwift
import RxSwift

class StorageService {
    private let realm: Realm!
    
    init() {
        self.realm = try! Realm()
    }
    
    func retrieveCameras() -> Observable<[CameraModel]> {
        return retrieveObjects()
    }
    
    func retrieveDoors() -> Observable<[DoorModel]> {
        return retrieveObjects()
    }
    
    func saveObject(_ object: Object) -> Single<Void> {
        return Single<Void>.create { single in
            try! self.realm.write {
                self.realm.add(object, update: .modified)
                single(.success(()))
            }
            
            return Disposables.create()
        }
    }
    
    func toggleFavorite(_ object: TableViewContent) -> Single<Void> {
        return Single<Void>.create { single in
            try! self.realm.write {
                object.toggleFavorite()
                single(.success(()))
            }
            
            return Disposables.create()
        }
    }
    
    func changeName(_ object: TableViewContent, newName: String) -> Single<Void> {
        return Single<Void>.create { single in
            try! self.realm.write {
                object.changeName(to: newName)
                single(.success(()))
            }
            
            return Disposables.create()
        }
    }
    
    private func retrieveObjects<T: Object>() -> Observable<[T]> {
        return Observable<[T]>.create { [self] observer -> Disposable in
            
            guard let objects = realm?.objects(T.self) else {
                observer.onError(ServiceError.noData)
                observer.onCompleted()
                return Disposables.create()
            }
            
            guard !objects.isEmpty else {
                observer.onError(ServiceError.empty)
                observer.onCompleted()
                return Disposables.create()
            }
            
            observer.onNext(Array(objects))
            observer.onCompleted()
            
            return Disposables.create()
        }
    }
}

extension StorageService {
    enum ServiceError: Error {
        case noData
        case empty
    }
}
