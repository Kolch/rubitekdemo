//
//  NetworkService.swift
//  RubitekDemo
//
//  Created by Alex Kolchedantsev on 10.02.2021.
//

import Foundation
import RxSwift

class NetworkService {
    
    var URLcomponents = URLComponents()
    let disposeBag = DisposeBag()
    
    init() {
        URLcomponents.scheme = "http"
        URLcomponents.host   = "cars.cprogroup.ru"
    }
    
    func fetchCameras() -> Observable<CamerasResponce> {
        URLcomponents.path = "/api/rubetek/cameras"
        return fetchListData(url: URLcomponents.url)
    }
    
    func fetchDoors() -> Observable<DoorsResponce> {
       URLcomponents.path = "/api/rubetek/doors"
        return fetchListData(url: URLcomponents.url)
    }
    
    private func fetchListData<T: NetworkResponce>(url: URL?) -> Observable<T> {
        guard let url = url else { return .error(ServiceError.urlRequest) }
        
        return URLSession.shared.rx.response(request: URLRequest(url: url))
            .map { result -> Data in
                guard result.response.statusCode == 200 else { throw ServiceError.urlRequest }
                return result.data
            }.map { data -> T in
                do {
                    let responceModel = try JSONDecoder().decode(T.self, from: data)
                    return responceModel
                } catch {
                    throw ServiceError.decode
                }
            }
            .observe(on: MainScheduler.instance)
            .asObservable()
    }
}

extension NetworkService {
    enum ServiceError: Error {
        case url(URLError)
        case urlRequest
        case decode
    }
}
