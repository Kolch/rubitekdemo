//
//  ImageLoader.swift
//  RubitekDemo
//
//  Created by Alex Kolchedantsev on 10.02.2021.
//

import Foundation
import UIKit.UIImage
import RxSwift

public final class ImageLoader {
    public static let shared = ImageLoader()

    private let cache: ImageCacheType
    private lazy var backgroundQueue: OperationQueue = {
        let queue = OperationQueue()
        queue.maxConcurrentOperationCount = 5
        return queue
    }()

    public init(cache: ImageCacheType = ImageCache()) {
        self.cache = cache
    }
    
    public func loadImage(from url: URL) -> Observable<UIImage?> {
        if let image = cache[url] {
            return .just(image)
        }
        
        return URLSession.shared.rx.response(request: URLRequest(url: url))
            .map { result -> Data in
                return result.data
            }.map { data -> UIImage? in
                return UIImage(data: data)
            }
            .observe(on: MainScheduler.instance)
            .asObservable()
    }
}
